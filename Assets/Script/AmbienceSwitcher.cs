﻿using FMODUnity;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Ambience
{
    OutdoorNight,
    Underwater,
    Cave
}

public class AmbienceSwitcher : Singleton<AmbienceSwitcher>
{
    #region Variables

    [UnityEngine.SerializeField, EventRef] private string m_ambienceSwitcherPath;
    private CustomEvent m_ambienceSwitcherEvent;

    private Ambience m_currentAmbience = Ambience.OutdoorNight;

    #endregion

    //--------

    #region Events
    private void OnEnable()
    {
        EventManager.SubscribeTo<bool>(EventID.OnFinishedLoadEvents, TakeEvents);
    }
    private void OnDisable()
    {
        EventManager.UnsubscribeFrom<bool>(EventID.OnFinishedLoadEvents, TakeEvents);
    }

    public void TakeEvents(bool inValue)
    {
        if (inValue)
        {
            SoundManager tempIstance = SoundManager.Instance;

            m_ambienceSwitcherEvent = tempIstance.GetEventFromDictionaryPath(m_ambienceSwitcherPath);
            m_ambienceSwitcherEvent.Play();
        }
    }
    #endregion

    #region Ambience event management

    /// <summary>
    /// Method used to change some soundtrack's params using <value string = "inParamName"/> & <value float = "inValue"/>
    /// </summary>
    public void ChangeParamToAmbience(string inParamName, float inValue)
    {
        //First it checks if the event has this param.
        if (m_ambienceSwitcherEvent.CheckParamInList(inParamName))
        {
            //Give to this param a new value.
            m_ambienceSwitcherEvent.ChangeParamFromName(inParamName, inValue);
        }
    }

    
    /// <summary>
    /// Method used to change some soundtrack's params using <value enum Ambience = "inNewAmbience"/>.
    /// </summary>
    public void ChangeAmbience(Ambience inNewAmbience)
    {
        if(inNewAmbience == Ambience.OutdoorNight)
        {
            m_ambienceSwitcherEvent.ChangeParamFromName("ambience_index", 0);
            m_ambienceSwitcherEvent.ChangeParamFromName("underwater", 0);
        }
        else if(inNewAmbience == Ambience.Cave)
        {
            m_ambienceSwitcherEvent.ChangeParamFromName("ambience_index", 1);
            m_ambienceSwitcherEvent.ChangeParamFromName("underwater", 0);
        }
        else if(inNewAmbience == Ambience.Underwater)
        {
            m_ambienceSwitcherEvent.ChangeParamFromName("underwater", 1);
        }
        m_currentAmbience = inNewAmbience;
    }
    #endregion
}
