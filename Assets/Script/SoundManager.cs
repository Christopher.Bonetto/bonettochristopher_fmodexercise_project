﻿using FMODUnity;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FMODUnity;
using FMOD.Studio;
using System.Linq;

public class SoundManager : Singleton<SoundManager>
{
    #region Variables

    private Dictionary<string, CustomEvent> m_eventsDictionary = new Dictionary<string, CustomEvent>();

    public Dictionary<string, Bus> BusesDictionary = new Dictionary<string, Bus>();

    [UnityEngine.SerializeField, EventRef] private string m_mainTrackPath;
    private CustomEvent m_mainTrackEvent;

    #endregion

    #region Behaviour cycle

    private void OnEnable()
    {
        EventManager.SubscribeTo<bool>(EventID.OnFinishedLoadEvents, TakeEvents);
    }
    private void OnDisable()
    {
        EventManager.UnsubscribeFrom<bool>(EventID.OnFinishedLoadEvents, TakeEvents);
    }

    #endregion

    #region CustomEvents
    private void TakeEvents(bool inValue)
    {
        if (inValue)
        {
            m_mainTrackEvent = GetEventFromDictionaryPath(m_mainTrackPath);
            m_mainTrackEvent.Play();
        }
    }

    #region Add event

    public void AddEventToDictionary(string inPath, CustomEvent inEvent)
    {
        m_eventsDictionary.Add(inPath, inEvent);
    }

    #endregion

    #region Get events

    public CustomEvent GetEventFromDictionaryIndex(int inValue)
    {
        return m_eventsDictionary.ElementAt(inValue).Value;
    }

    public CustomEvent GetEventFromDictionaryPath(string inPath)
    {
        if (m_eventsDictionary.ContainsKey(inPath))
        {
            return m_eventsDictionary[inPath];
        }
        return null;
    }

    #endregion

    #region Release events

    public void ReleaseEventsFromDicionary()
    {
        foreach(string key in m_eventsDictionary.Keys)
        {
            m_eventsDictionary[key].EventDescription.releaseAllInstances();
            m_eventsDictionary[key].EventDescription.unloadSampleData();
        }
        m_eventsDictionary.Clear();
    }

    #endregion

    #endregion

    #region Bus

    #region Get bus

    public Bus? GetBusFromName(string inName)
    {
        if (BusesDictionary.ContainsKey(inName))
        {
            return BusesDictionary[inName];
        }

        return null;
    }

    #endregion

    #region Set Bus

    public void SetBusValue(string inName, float inValue)
    {
        if (GetBusFromName(inName).HasValue)
        {
            inValue = Mathf.Clamp(inValue, 0, 1);
            BusesDictionary[inName].setVolume(inValue);
        }
    }

    public void SetSoundsBusVolume(float inValue)
    {
        SetBusValue("Sound", inValue);
    }

    public void SetMusicBusVolume(float inValue)
    {
        SetBusValue("Music", inValue);
    }

    public void SetAllAmbienceBusVolume(float inValue)
    {
        SetBusValue("Sound/Background", inValue);
    }

    public void SetFoleyBusVolume(float inValue)
    {
        SetBusValue("Sound/Foley", inValue);
    }

    public void SetSfxBusVolume(float inValue)
    {
        SetBusValue("Sound/SFX", inValue);
    }

    public void SetAmbienceBusVolume(float inValue)
    {
        SetBusValue("Sound/Background/Ambience", inValue);
    }

    public void SetUnderwaterBusVolume(float inValue)
    {
        SetBusValue("Sound/Background/Underwater", inValue);
    }

    #endregion

    #endregion
}
